import {Component} from '@angular/core';
import {SocialTagsCustomElement} from '@set/social-tags-custom-element';

import {LOGIN_ATI_SELECTOR} from '../../custom-elements/custom-selectors';
import {ActionsDataService} from '../../services/actions-data.service';
import {OAuthService} from '../../services/oauth.service';

@Component({
  selector: LOGIN_ATI_SELECTOR,
  templateUrl: './mr-login.component.html',
  styleUrls: ['./mr-login.component.scss']
})
@SocialTagsCustomElement(LOGIN_ATI_SELECTOR)
export class MrLoginComponent {
  public isLoggedIn: boolean = false;
  public openLogin: boolean;

  constructor(private _oAuthService: OAuthService, private _actionDataService: ActionsDataService) {
    this.openLogin = true;
  }

  public login(): void {
    this.openLogin = false;
    this._oAuthService.login();
  }

  public closePopup(): void {
    this.openLogin = false;
  }
}
